# QaProgram.Test.UI.Cypress

**Dependências**
- Node 12.x.x
- npm 6.12.x
- Chrome browser

**Configuração**

Dependencias: Acessar a pasta do projeto via terminal e executar o comando: `npm install`

**Executar Testes**

UI: Acessar a pasta do projeto via terminal e executar o comando: `npm test`

**Visualizar o relatório**

Abrir o arquivo ./cypress/reports/MultipleReport/index.html no navegador ou
./cypress/reports/SingleReport.html (A partir da pasta raiz do projeto)               
