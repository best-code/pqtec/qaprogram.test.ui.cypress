import HomePage from "../../support/webPage/homePage";
import {And, Given, Then} from "cypress-cucumber-preprocessor/steps";
import ArticlePage from "../../support/webPage/articlePage";

Given('The user click on new article button', ()=>{
    HomePage.clickOnNewArticleLink();
})

And('Add a title to the article as {string}', (title)=>{
    HomePage.insertNewArticleTitle(title);
})

And('Add a description the article as {string}', (description)=>{
    HomePage.insertNewArticleDescription(description)
})

And('Write the article text as {string}', (text)=>{
    HomePage.insertNewArticleTextArea(text)
})

And('Enter the tags list as {string}', (tagList)=>{
    HomePage.insertNewArticleTagList(tagList)
})

And('Click on the Publish Article button', ()=>{
    HomePage.clickOnNewArticlePublishButton()
})

And('I do not fill in any field like title, what the article is about, article and tags', ()=>{

})

Then('The user lands on article page', (title)=>{
    ArticlePage.verifyArticleTitle('The life of ninja turtles');
})

Then('The message {string} is displayed under the fields', (message)=>{
    HomePage.verifyAlertMessage(message);
})