import SignupPage from "../../support/webPage/signupPage";
import HomePage from "../../support/webPage/homePage";
import { Given, And, Then } from "cypress-cucumber-preprocessor/steps";
import {faker} from "@faker-js/faker";

let username

Given('The user is on the signup page', ()=>{
    HomePage.clickOnSignup();
    SignupPage.verifyLandingPage();
})

And('Insert a user name', ()=>{
    username = faker.name.firstName();
    SignupPage.insertUserName(username);
})

And('Insert a email', ()=>{
    let email = faker.internet.email();
    SignupPage.insertEmail(email);
})

And('Insert a password', ()=>{
    let password = faker.internet.password();
    SignupPage.insertPassword(password);
})

And('Click on sign up button', ()=>{
    SignupPage.clickOnSubmit();
})

Then('The user lands on home page', ()=>{
    HomePage.verifyLogged(username);
})