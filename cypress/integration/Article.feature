Feature: Article

  Background:
    Given The user is on the signup page
    And Insert a user name
    And Insert a email
    And Insert a password
    When Click on sign up button
    Then The user lands on home page

  Scenario: Create article
    Given The user click on new article button
    And Add a title to the article as "The life of ninja turtles"
    And Add a description the article as "In this article we will describe how the ninja turtles were born and lived"
    And Write the article text as "The ninja turtles were born through a laboratory experiment that didn't go very well, then they went to live in the city's sewers."
    And Enter the tags list as "Ninja turtles"
    When Click on the Publish Article button
    Then The user lands on article page

  Scenario: Validate mandatory fields
    Given The user click on new article button
    And  I do not fill in any field like title, what the article is about, article and tags
    When Click on the Publish Article button
    Then The message "This field is required" is displayed under the fields