
export const getUserNameInputField = () => cy.get("input[name='username']")
export const getEmailInputField = () => cy.get("input[name='email']")
export const getPasswordInputField = () => cy.get("input[name='password']")
export const getSubmitButton = () => cy.get("button")
export const getSignupTitle = () => cy.get("h1.text-xs-center")


class SignupPage{

    clickOnSubmit(){
        getSubmitButton().should('be.visible').click();
    }

    insertUserName(name){
        getUserNameInputField().clear().type(name);
    }

    insertEmail(email){
        getEmailInputField().clear().type(email);
    }

    insertPassword(password){
        getPasswordInputField().clear().type(password);
    }

    verifyLandingPage(){
        getSignupTitle().should('have.text', 'Sign up')
    }

} export default new SignupPage

