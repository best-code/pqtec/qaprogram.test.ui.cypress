import {getEmailInputField, getPasswordInputField, getSubmitButton, getUserNameInputField} from "./signupPage";

export const getSignupLink = () => cy.get("a[href='#/register']")
export const getUserNameMenu = () => cy.get("ul.navbar-nav li:last-child a.nav-link")
export const getNewArticleLink = () => cy.get("a[routerlink$='/editor']")
export const getNewArticleTitle = () => cy.get("input[name='title']")
export const getNewArticleDescription = () => cy.get("input[name='description']")
export const getNewArticleTagList = () => cy.get("input[name='tagList']")
export const getNewArticleTextArea = () => cy.get("textarea[name='body']")
export const getNewArticlePublishButton = () => cy.get("button[type='submit']")
export const getAlertMessage = () => cy.get(".ng-star-inserted")

class HomePage{

    clickOnSignup(){
        getSignupLink().should('be.visible').click();
    }

    verifyLogged(username){
        getUserNameMenu().should('contain.text', username)
    }

    clickOnNewArticleLink(){
        getNewArticleLink().should('be.visible').click();
    }

    clickOnNewArticlePublishButton(){
        getNewArticlePublishButton().should('be.visible').click();
    }

    verifyAlertMessage(message){
        getAlertMessage().should('contain.text',message);
    }

    insertNewArticleTitle(title){
        getNewArticleTitle().clear().type(title);
    }

    insertNewArticleDescription(description){
        getNewArticleDescription().clear().type(description);
    }

    insertNewArticleTagList (tagList){
        getNewArticleTagList().clear().type(tagList);
    }

    insertNewArticleTextArea (text){
        getNewArticleTextArea().clear().type(text);
    }

} export default new HomePage