
export const getArticleTitle = () => cy.get("h1")


class ArticlePage{

   verifyArticleTitle(title){
       getArticleTitle().should('have.text', title)
    }

} export default new ArticlePage