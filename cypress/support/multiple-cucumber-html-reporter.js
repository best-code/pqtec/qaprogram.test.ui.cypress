const report = require('multiple-cucumber-html-reporter');

var today = new Date();
var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
var time = today.getHours() + "h" + today.getMinutes() + "m" + today.getSeconds() + "s";
var dateTime = date+'_'+time;

report.generate({
    jsonDir: 'cypress/cucumber-json/',
    reportPath: "cypress/reports/MultipleReport",
    metadata:{
        browser: {
            name: 'Chrome',
            version: '+100'
        },
        device: 'Local',
        platform: {
            name: 'Windows',
            version: '11'
        }
    },
    customData: {
        title: 'Conduit',
        data: [
            {label: 'Project', value: 'Conduit'},
            {label: 'Release', value: '1.0'}
        ]
    }
});